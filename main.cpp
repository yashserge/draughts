#include <QApplication>
#include "view.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Game game;
    View view;

    view.setGame(&game);
    view.show();
    
    return app.exec();
}
