#include "view.h"
#include "ui_view.h"
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>

View::View(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::View)
{
    ui->setupUi(this);
    this->setWindowTitle("Draughtsmans");

    QTimer* updater = new QTimer(this);
    updater->start(30);

    connect(ui->pbPlay, SIGNAL(clicked()), SLOT(pbPlayClicked()));
    connect(updater, SIGNAL(timeout()), SLOT(update()));
}

void View::setGame(Game *game)
{
    this->game = game;    
}

View::~View()
{
    delete ui;
}

void View::mousePressEvent(QMouseEvent *e)
{
    if (e->button() != Qt::LeftButton)
        return;

    if (!this->game->isActive() || !this->game->isPlayersTurn())
        return;

    this->realMousePosition = e->pos();
    int draughtsmanIndex = this->game->getDraughtsmanIndexOnPosition((e->pos() - QPoint(25, 25)) / 50);
    if (draughtsmanIndex < 0)
        return;

    if (this->game->getDraughtsmanType(draughtsmanIndex) != this->game->getPlayMode())
        return;

    this->game->setSelectedDraughtsmanIndex(draughtsmanIndex);
}

void View::mouseMoveEvent(QMouseEvent *e)
{
    this->realMousePosition = e->pos();
    this->mousePosition = (e->pos() - QPoint(25, 25)) / 50;
}

void View::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() != Qt::LeftButton)
        return;

    if (!this->game->isActive())
        return;

    this->realMousePosition = e->pos();
    this->game->moveSelectedDraughtsmanToPosition((e->pos() - QPoint(25, 25)) / 50);
    this->game->setSelectedDraughtsmanIndex(-1);

    Game::DraughtsmanType winner;
    if (this->game->isGameOver(winner))
        QMessageBox::information(NULL, "Attention!", (winner == this->game->getPlayMode() ? "You win" : "Game over"));

}

void View::pbPlayClicked()
{
    this->ui->pbPlay->setText("Restart");

    this->game->setPlayMode(ui->rbWhite->isChecked() ? Game::MT_WHITE : Game::MT_BLACK);
    this->game->setAILevel(ui->sbAILevel->value());
    this->game->reset();
}

void View::paintEvent(QPaintEvent*)
{    
    QPainter p(this);
    p.setPen(Qt::NoPen);

    //draw grid
    p.setBrush(QBrush(QColor(40,40,40)));
    for (int i = 0; i < 8; i++)
        for (int k = 0; k < 8; k++)
            if ((i + k) % 2 != 0) p.drawRect(i * 50, k * 50, 50, 50);

    p.setBrush(QBrush(QColor(140,100,60)));
    for (int i = 0; i < 8; i++)
        for (int k = 0; k < 8; k++)
            if ((i + k) % 2 == 0) p.drawRect(i * 50, k * 50, 50, 50);

    p.setPen(Qt::SolidLine);
    p.drawLine(400, 0, 400, 400);

    //draw draughtsmans
    for (int i = 0; i < this->game->getDraughtsmanCount(); i++)
        if (i != this->game->getSelectedDraughtsmanIndex() && this->game->isInGame(i))
        {
            p.setBrush(QBrush(QColor(this->game->getDraughtsmanType(i) == Game::MT_BLACK ? 125 : 210,
                                     this->game->getDraughtsmanType(i) == Game::MT_BLACK ? 125 : 210,
                                     this->game->getDraughtsmanType(i) == Game::MT_BLACK ? 125 : 210)));
            p.drawEllipse(this->game->getDraughtsmanPosition(i) * 50 + QPoint(25,25) , 20, 20);
        }


    if (this->game->getSelectedDraughtsmanIndex() >= 0)
        if (!(this->mousePosition.x() < 0 || this->mousePosition.y() < 0 || this->mousePosition.x() > 7 || this->mousePosition.y() > 7))
        {
            if (this->game->canMoveToPosition(this->game->getSelectedDraughtsmanIndex(), this->mousePosition) ||
                this->game->canJumpToPosition(this->game->getSelectedDraughtsmanIndex(), this->mousePosition))
            {
                p.setPen(QPen(QBrush(Qt::white), 5));
                p.setBrush(QBrush(QColor(210, 210, 210), Qt::NoBrush));
                p.drawRect(QRect(this->mousePosition * 50, QSize(50, 50)));
            }
        }

    if (game->getSelectedDraughtsmanIndex() >= 0)
    {
        p.setPen(Qt::NoPen);
        p.setBrush(QBrush(QColor(this->game->getDraughtsmanType(this->game->getSelectedDraughtsmanIndex()) == Game::MT_BLACK ? 155 : 255,
                                 this->game->getDraughtsmanType(this->game->getSelectedDraughtsmanIndex()) == Game::MT_BLACK ? 155 : 255,
                                 this->game->getDraughtsmanType(this->game->getSelectedDraughtsmanIndex()) == Game::MT_BLACK ? 155 : 255)));
        p.drawEllipse(this->realMousePosition , 20, 20);
    }
}

