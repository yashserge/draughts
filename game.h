#ifndef GAME_H
#define GAME_H

#include <iostream>

#include <QObject>
#include <QPoint>
#include <QQueue>
#include <QList>

#define MIN_VALUE 0
#define MAX_VALUE 255
#define EPIC_BIG_VALUE 500

class Game
{
public:
    Game();

    // Типы шашек
    enum DraughtsmanType {
        MT_NO_ONE = 0,
        MT_WHITE = 1,
        MT_BLACK = 2
    };

    // Общее количество шашек игроков
    int getDraughtsmanCount() { return 24; }

    // Функции запуска игры и проверки, продолжается ли она
    void setActive(bool active) { this->active = active; }
    bool isActive() { return this->active; }

    // Установка сложности
    void setAILevel(int AILevel) { this->AILevel = AILevel; }

    // Установка и получение режимов игры: черные или белые
    void setPlayMode(DraughtsmanType gameMode) { this->gameMode = gameMode;  }
    DraughtsmanType getPlayMode() { return this->gameMode; }

    // Получить положение белой шашки по ее номеру
    QPoint& getWhitePosition(int index)
    {
        Q_ASSERT(index >= 0 && index < 12);
        return whites[index];
    }

    // Получить положение черной шашки по ее номеру
    QPoint& getBlackPosition(int index)
    {
        Q_ASSERT(index >= 0 && index < 12);
        return blacks[index];
    }

    // Проверить диапазон по двум координатам или точке
    bool checkRange(int x, int y) { return (x >= 0 && y >= 0 && x < 8 && y < 8); }
    bool checkRange(const QPoint &point) { return checkRange(point.x(), point.y()); }

    // Получить точку по номеру шашки
    QPoint& getDraughtsmanPosition(int draughtsmanIndex)
    {
        if (draughtsmanIndex >= 0 && draughtsmanIndex < 12)
            return getWhitePosition(draughtsmanIndex);
        else
            return getBlackPosition(draughtsmanIndex - 12);
    }

    // Получить тип шашки по ее номеру
    DraughtsmanType getDraughtsmanType(int draughtsmanIndex)
    {
        if (draughtsmanIndex >= 0 && draughtsmanIndex < 12)
            return MT_WHITE;
        else
            return MT_BLACK;
    }

    // Получить и установить номер выборанной игроком шашки
    int getSelectedDraughtsmanIndex() { return selectedDraughtsman;  }
    void setSelectedDraughtsmanIndex(int draughtsmanIndex) { this->selectedDraughtsman = draughtsmanIndex; }

    // Проверить, сейчас ли ход пользователя
    int isPlayersTurn() { return playersTurn; }

    // Проверить, в игре ли шашка
    bool isInGame(int draughtsmanIndex)
    {
        Q_ASSERT(draughtsmanIndex >= 0 && draughtsmanIndex < 24);

        if (draughtsmanIndex >= 0 && draughtsmanIndex < 12)
            return whiteStatus[draughtsmanIndex];
        else
            return blackStatus[draughtsmanIndex - 12];
    }

    // Сбросить игру на начало
    void reset();

    // Получить номер шашки по позиции на доске (точке)
    int getDraughtsmanIndexOnPosition(const QPoint &pos);

    // Проверить, может ли данная шашка сходить на данную позицию: обычным ходом или прыжком
    bool canMoveToPosition(int index, const QPoint &pos);
    bool canJumpToPosition(int index, const QPoint &pos);

    // Переместить выбранную пользоватетем шашку в указанное мышью место
    bool moveSelectedDraughtsmanToPosition(const QPoint &pos);

    // Проверить, закончилась ли игра
    bool isGameOver(DraughtsmanType &winner);
    bool isGameOver() { DraughtsmanType winner; return isGameOver(winner); }

private:
    static const int NOT_INITIALIZED = 255;

    static const int EMPTY = 0;
    static const int WHITE = 1;
    static const int BLACK = 255;

    // Позиции черных и белых шашек
    QPoint blacks[12];
    QPoint whites[12];

    // Их статусы (в игре или выбыли)
    int blackStatus[12];
    int whiteStatus[12];

    // Режим игры (за белых/черных)
    DraughtsmanType gameMode;

    // Карта для построение ходов компьютером
    int map[8][8];

    // Всевозможные ходы фишек
    QPoint possibleMoves[8];

    // Флаг активности игры
    bool active;
    // Флаг хода игрока
    bool playersTurn;
    // Уровень сложности (глубина построения дерева ходов)
    int AILevel;
    // Номер Выбранной пользователем шашки
    int selectedDraughtsman;

    // Функция инициализации игры (доски и фишек)
    void initialize();

    // Функция проверки, свободна ли клетка
    bool isFree(int x, int y);
    bool isFree(const QPoint &point) { return isFree(point.x(), point.y()); }

    // Функция эвристики
    int getHeuristicEvaluation();

    // МинМакс-функция с альфе-бета отсечением
    int runMinMax(DraughtsmanType draughtsman, int recursiveLevel, int alpha, int beta);

    // Функции для временного перемещения фишки
    void temporaryDraughtsmanMovement(int draughtsmanIndex, int x, int y);
    void temporaryDraughtsmanMovement(int draughtsmanIndex, const QPoint &point) { temporaryDraughtsmanMovement(draughtsmanIndex, point.x(), point.y()); }
    void temporaryDraughtsmanJump(int draughtsmanIndex, const QPoint &point, bool restore = false);

    // Функция подготовки карты для Мин-Макс функции
    void prepareMap();

    // Отладочная печать состояний всех фишек
    void debugPrint();
};

#endif // GAME_H
