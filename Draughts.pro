QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Draughts

TEMPLATE = app

SOURCES += main.cpp\
           view.cpp\
           game.cpp

HEADERS += view.h\
           game.h

FORMS += view.ui
