#include <iostream>

#include "game.h"
#include "qmath.h"

// Конструктор
Game::Game()
    :active(false), selectedDraughtsman(-1)
{
    // Перемещения для черных (вниз)
    possibleMoves[0] = QPoint( -1, +1);
    possibleMoves[1] = QPoint( +1, +1);

    // Перемещения для белых (вверх)
    possibleMoves[2] = QPoint( -1, -1);
    possibleMoves[3] = QPoint( +1, -1);

    // Перемещения для битья вражеских шашек (для всех во все стороны)
    possibleMoves[4] = QPoint( -2, +2);
    possibleMoves[5] = QPoint( +2, +2);
    possibleMoves[6] = QPoint( -2, -2);
    possibleMoves[7] = QPoint( +2, -2);

    // Подготовка доски и шашек
    initialize();
}

// Сброс игры
void Game::reset()
{
    // Игра идет и усановка начальных параметров
    this->active = true;
    initialize();

    // Запуск хода компьютера, если необходимо
    if (this->gameMode != MT_WHITE)
        runMinMax(MT_WHITE, 0, -EPIC_BIG_VALUE, +EPIC_BIG_VALUE);
        //runMinMax((gameMode == MT_WHITE) ? MT_BLACK : MT_WHITE, 0, -EPIC_BIG_VALUE, +EPIC_BIG_VALUE);

    // Установка хода игрока
    this->playersTurn = true;
}

int Game::getDraughtsmanIndexOnPosition(const QPoint &pos)
{
    for (int i = 0; i < 12; i++)
    {
        // Ищем среди белых
        if (this->whites[i] == pos)
           return i;

        // Ищем среди черных
        if (this->blacks[i] == pos)
           return i + 12;
    }

    return -1;
}

//
bool Game::canMoveToPosition(int draughtsmanIndex, const QPoint &pos)
{
    Q_ASSERT(draughtsmanIndex >= 0 && draughtsmanIndex < 24);

    // Новая точка на доске
    if (!checkRange(pos))
        return false;

    // Новая точка на черной клетке
    if ((pos.x() + pos.y()) % 2 == 0)
        return false;

    QPoint oldPosition = getDraughtsmanPosition(draughtsmanIndex);
    QPoint diff = oldPosition - pos;

    // Расстояние до новой точки равно 1 по каждой оси
    if (abs(diff.x()) != 1 || abs(diff.y()) != 1)
        return false;

    // Других шашек нет на новой точке
    for (int i = 0; i < getDraughtsmanCount(); i++)
        if (i != draughtsmanIndex && isInGame(i) && getDraughtsmanPosition(i) == pos)
            return false;

    // Черная шашка перемещается вниз
    if (draughtsmanIndex >= 12 && oldPosition.y() > pos.y())
        return false;

    // Белая шашка перемещается вверх
    if (draughtsmanIndex < 12 && oldPosition.y() < pos.y())
        return false;

    return true;
}

bool Game::canJumpToPosition(int draughtsmanIndex, const QPoint &pos)
{
    Q_ASSERT(draughtsmanIndex >= 0 && draughtsmanIndex < 24);

    // Новая точка на доске
    if (!checkRange(pos))
        return false;

    // Новая точка на черной клетке
    if ((pos.x() + pos.y()) % 2 == 0)
        return false;

    QPoint oldPosition = getDraughtsmanPosition(draughtsmanIndex);
    QPoint diff = pos - oldPosition;

    // Расстояние до новой точки равно 2 по каждой оси
    if (abs(diff.x()) != 2 || abs(diff.y()) != 2)
        return false;

    // Других шашек нет на новой точке
    for (int i = 0; i < getDraughtsmanCount(); i++)
        if (i != draughtsmanIndex && isInGame(i) && getDraughtsmanPosition(i) == pos)
            return false;

    QPoint mid = oldPosition + diff / 2;

    // На пути есть шашка
    if (getDraughtsmanIndexOnPosition(mid) == -1)
        return false;

    // На пути черной шашки есть белая шашка
    if (draughtsmanIndex >= 12 && getDraughtsmanIndexOnPosition(mid) >= 12)
        return false;

    // На пути белой шашки есть черная шашка
    if (draughtsmanIndex < 12 && getDraughtsmanIndexOnPosition(mid) < 12)
        return false;

    return true;
}

bool Game::isGameOver(DraughtsmanType &winner)
{
    winner = MT_NO_ONE;

    // Проверяем, может ли двигаться хоть одна живая черная шашка
    bool canMove = false;
    for (int k = 0; k < 12; k++)
    {
        if (this->blackStatus[k])
        {
            for (int i = 4; i < 8; i++)
                if (canJumpToPosition(k + 12, this->blacks[k] + this->possibleMoves[i]))
                   canMove = true;

            for (int i = 0; i < 2; i++)
                if (canMoveToPosition(k + 12, this->blacks[k] + this->possibleMoves[i]))
                   canMove = true;
        }
    }

    // Если не может, по предваритально устанавливаем выигрыш белым
    if (!canMove)
        winner = MT_WHITE;

    // Проверяем, может ли двигаться хоть одна живая белая шашка
    canMove = false;
    for (int k = 0; k < 12; k++)
    {
        if (this->whiteStatus[k])
        {
            for (int i = 4; i < 8; i++)
                if (canJumpToPosition(k, this->whites[k] + this->possibleMoves[i]))
                   canMove = true;

            for (int i = 2; i < 4; i++)
                if (canMoveToPosition(k, this->whites[k] + this->possibleMoves[i]))
                    canMove = true;
        }
    }

    // Если беллые не двигаются, а двигаются черные, то победитель - черные, иначе - белые
    if (!canMove && winner == MT_NO_ONE)
        winner = MT_BLACK;

    // Если победитль есть, останавливаем игру
    if (winner != MT_NO_ONE)
    {
        this->active = false;
        return true;
    }

    return false;
}

bool Game::moveSelectedDraughtsmanToPosition(const QPoint &pos)
{
    // Если выбрана шашка
    if (this->selectedDraughtsman < 0)
        return false;

    QPoint oldPosition = getDraughtsmanPosition(this->selectedDraughtsman);

    // Проверяем, может ли она побить кого-нибудь
    if (canJumpToPosition(this->selectedDraughtsman, pos))
    {
        QPoint diff = pos - oldPosition;
        QPoint mid = oldPosition + diff / 2;
        int midIndex = getDraughtsmanIndexOnPosition(mid);

        std::cout << "user jumps " << this->selectedDraughtsman
                  << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                  << " to (" << pos.x() << ", " << pos.y() << ") and removes "
                  << midIndex << std::endl;

        // Бьем промежуточную шашку
        if (this->selectedDraughtsman >= 0 && this->selectedDraughtsman < 12)
        {
            this->whites[this->selectedDraughtsman] = pos;
            this->blackStatus[midIndex - 12] = 0;
            this->blacks[midIndex - 12] = QPoint(-1, -1);
        }
        else
        {
            this->blacks[this->selectedDraughtsman - 12] = pos;
            this->whiteStatus[midIndex] = 0;
            this->whites[midIndex] = QPoint(-1, -1);
        }
    } // Иначе проверяем, может ли она сходить
    else if (canMoveToPosition(this->selectedDraughtsman, pos))
    {
        std::cout << "user moves " << this->selectedDraughtsman
                  << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                  << " to (" << pos.x() << ", " << pos.y() << ")" << std::endl;

        // Ходим на новое место
        if (this->selectedDraughtsman >= 0 && this->selectedDraughtsman < 12)
            this->whites[this->selectedDraughtsman] = pos;
        else
            this->blacks[this->selectedDraughtsman - 12] = pos;
    }
    else
        return false;

    debugPrint();

    // Проверяем, не закончилась ли игра
    if (isGameOver())
        return true;

    // Меняем игроков и запускаем ход компьютера
    this->playersTurn = !this->playersTurn;
    runMinMax(this->gameMode == MT_WHITE ? MT_BLACK : MT_WHITE, 0, -EPIC_BIG_VALUE, +EPIC_BIG_VALUE);

    debugPrint();

    // Меняем игроков и проверяем, не закончилась ли игра
    this->playersTurn = !this->playersTurn;
    isGameOver();

    return true;
}

void Game::prepareMap()
{
    // Устанавливаем всю карту пустой
    for (int i = 0; i < 8; i++)
        memset(this->map[i], 0, 8 * sizeof(int));

    for (int i = 0; i < 12; i++)
    {
        // Помещаем на карту живих белых
        if (this->whiteStatus[i])
            this->map[this->whites[i].y()][this->whites[i].x()] = WHITE;

        // Помещаем на карту живих черных
        if (this->blackStatus[i])
            this->map[this->blacks[i].y()][this->blacks[i].x()] = BLACK;
    }
}

int Game::getHeuristicEvaluation()
{
    // Функция эвристики - разность черных шашек и белых
    int minMax = 12;

    for (int i = 0; i < 12; i++)
        if (this->blackStatus[i])
            minMax += 1;

    for (int i = 0; i < 12; i++)
        if (this->whiteStatus[i])
            minMax -= 1;

    return minMax;
}

void Game::temporaryDraughtsmanMovement(int draughtsmanIndex, int x, int y)
{
    // Перемещаем белую шашку
    if (draughtsmanIndex >= 0 && draughtsmanIndex < 12)
    {
        this->map[this->whites[draughtsmanIndex].y()][this->whites[draughtsmanIndex].x()] = EMPTY;
        this->map[this->whites[draughtsmanIndex].y() + y][this->whites[draughtsmanIndex].x() + x] = WHITE;
        this->whites[draughtsmanIndex] += QPoint(x, y);
    }
    else
    {// Перемещаем черную шашку
        this->map[this->blacks[draughtsmanIndex - 12].y()][this->blacks[draughtsmanIndex - 12].x()] = EMPTY;
        this->map[this->blacks[draughtsmanIndex - 12].y() + y][this->blacks[draughtsmanIndex - 12].x() + x] = BLACK;
        this->blacks[draughtsmanIndex - 12] += QPoint(x, y);
    }
}

void Game::temporaryDraughtsmanJump(int draughtsmanIndex, const QPoint &move, bool restore)
{
    // Прыгаем белой шашкой
    if (draughtsmanIndex >= 0 && draughtsmanIndex < 12)
    {
        this->map[this->whites[draughtsmanIndex].y()][this->whites[draughtsmanIndex].x()] = EMPTY;
        this->map[this->whites[draughtsmanIndex].y() + move.y()][this->whites[draughtsmanIndex].x() + move.x()] = WHITE;
        this->whites[draughtsmanIndex] += move;

        QPoint mid = this->whites[draughtsmanIndex] - move / 2;
        int midIndex = getDraughtsmanIndexOnPosition(mid);

        // Востанаввливаем убитую промежуточную шашку
        if (restore)
        {
            blackStatus[midIndex - 12] = 1;
            this->map[mid.y()][mid.x()] = BLACK;
        }// Убиваем промежуточную шашку
        else
        {
            blackStatus[midIndex - 12] = 0;
            this->map[mid.y()][mid.x()] = EMPTY;
        }
    }
    else
    {// Прыгаем черной шашкой
        this->map[this->blacks[draughtsmanIndex - 12].y()][this->blacks[draughtsmanIndex - 12].x()] = EMPTY;
        this->map[this->blacks[draughtsmanIndex - 12].y() + move.y()][this->blacks[draughtsmanIndex - 12].x() + move.x()] = BLACK;
        this->blacks[draughtsmanIndex - 12] += move;

        QPoint mid = this->blacks[draughtsmanIndex - 12] - move / 2;
        int midIndex = getDraughtsmanIndexOnPosition(mid);

        // Востанаввливаем убитую промежуточную шашку
        if (restore)
        {
            whiteStatus[midIndex] = 1;
            this->map[mid.y()][mid.x()] = WHITE;
        }// Убиваем промежуточную шашку
        else
        {
            whiteStatus[midIndex] = 0;
            this->map[mid.y()][mid.x()] = EMPTY;
        }
    }
}

bool Game::isFree(int x, int y)
{
    // Координаты на доске
    if (!checkRange(x,y))
        return false;

    // На доске в клетке никого нет
    if (map[y][x] != EMPTY)
        return false;

    return true;
}

int Game::runMinMax(DraughtsmanType draughtsman, int recursiveLevel, int alpha, int beta)
{
    //if (!recursiveLevel)
    //    std::cout << "  first run min-max with level " << recursiveLevel << std::endl;
    //else
    //    std::cout << "    run min-max with level " << recursiveLevel << std::endl;

    if (recursiveLevel == 0)
        this->prepareMap();

    int test = NOT_INITIALIZED;

    // Выходим из рекурсии, если построили все уровни, и возвращаем функцию эвристики для текущегго состояния
    if (recursiveLevel >= this->AILevel * 2)
    {
        //std::cout << "  end recursion with level " << recursiveLevel << " and AI-level " << this->AILevel << std::endl;

        int heuristic =  getHeuristicEvaluation();
        prepareMap();

        return heuristic;
    }

    // Инициализируем лучший ход начальным значением
    int bestMove = NOT_INITIALIZED;

    // Выбираем фунцию оценки: поиск минимума или максимума
    bool isBlack = (draughtsman == MT_BLACK);
    int MinMax = isBlack ? MIN_VALUE : MAX_VALUE;

    // Рассматриваем всевозможные ходы для шашек
    for (int i = 0; i < (48 + 24); i++)
    {
        // Если ход прыжка
        if (i < 48)
        {
            // try jump
            int curDraughtsman = i / 4 + (isBlack ? 12 : 0);

            if (isInGame(curDraughtsman))
            {
                QPoint curDraughtsmanPos = getDraughtsmanPosition(curDraughtsman);
                QPoint curMove = this->possibleMoves[4 + i % 4];

                //std::cout << "  " << curDraughtsman << " in game at (" << curDraughtsmanPos.x() << ", "
                //          << curDraughtsmanPos.y() << ") at checking jump" << std::endl;

                // Если можно прыгать
                if (canJumpToPosition(curDraughtsman, curDraughtsmanPos + curMove))
                {
                    //std::cout << "    bot can jump " << curDraughtsman << " to ("
                    //          << (curDraughtsmanPos + curMove).x() << ", "
                    //          << (curDraughtsmanPos + curMove).y() << ")" << std::endl;

                    // Прыгаем, запускаем дальше функцию и возвращаем предыдущее состояние до прыжка
                    temporaryDraughtsmanJump(curDraughtsman, curMove);
                    test = runMinMax(isBlack ? MT_WHITE : MT_BLACK, recursiveLevel + 1, alpha, beta);
                    temporaryDraughtsmanJump(curDraughtsman, -curMove, true);

                    // Устанавливаем наилучший ход
                    if ((test > MinMax && draughtsman == MT_BLACK) || (test <= MinMax && draughtsman == MT_WHITE) || (bestMove == NOT_INITIALIZED))
                    {
                        MinMax = test;
                        bestMove = i;
                    }

                    //std::cout << "  alpha " << alpha << ", beta " << beta << std::endl;

                    // Меняем границы, если нужно
                    if (isBlack)
                        alpha = qMax(alpha, test);
                    else
                        beta = qMin(beta, test);

                    //std::cout << "  new alpha " << alpha << ", new beta " << beta << std::endl;

                    // Если по текущим границам ясно, что дальше смотреть бессмысленно, прекращаем
                    if (beta < alpha)
                       break;
                }
            }
        }
        else
        {// Если обычный ход
            // try move
            int curDraughtsman = (i - 48) / 2 + (isBlack ? 12 : 0);

            if (isInGame(curDraughtsman))
            {
                QPoint curDraughtsmanPos = getDraughtsmanPosition(curDraughtsman);
                QPoint curMove = this->possibleMoves[isBlack ? i % 2 : i % 2 + 2];

                //std::cout << "  " << curDraughtsman << " in game at (" << curDraughtsmanPos.x() << ", "
                //          << curDraughtsmanPos.y() << ") at checking move" << std::endl;

                //if (curDraughtsmanPos.x() == -1)
                //    std::cout << isInGame(curDraughtsman) << std::endl;

                // Если клетка свободна
                if (isFree(curDraughtsmanPos + curMove))
                {
                    //std::cout << "    bot can move " << curDraughtsman << " to ("
                    //          << (curDraughtsmanPos + curMove).x() << ", "
                    //          << (curDraughtsmanPos + curMove).y() << ")" << std::endl;

                    // Ходим, запускаем дальше функцию и возвращаем предыдущее состояние до шага
                    temporaryDraughtsmanMovement(curDraughtsman, curMove);
                    test = runMinMax(isBlack ? MT_WHITE : MT_BLACK, recursiveLevel + 1, alpha, beta);
                    temporaryDraughtsmanMovement(curDraughtsman, -curMove);

                    // Проверяем наилучший ход
                    if ((test > MinMax && draughtsman == MT_BLACK) || (test <= MinMax && draughtsman == MT_WHITE) || (bestMove == NOT_INITIALIZED))
                    {
                        MinMax = test;
                        bestMove = i;
                    }

                    //std::cout << "  alpha " << alpha << ", beta " << beta << std::endl;

                    // Меняем границы, если нужно
                    if (isBlack)
                        alpha = qMax(alpha, test);
                    else
                        beta = qMin(beta, test);

                    //std::cout << "  new alpha " << alpha << ", new beta " << beta << std::endl;

                    // Если по текущим границам ясно, что дальше смотреть бессмысленно, прекращаем
                    if (beta < alpha)
                       break;
                }
            }
        }
    }

    // Если не нашли лучшего хода, то возвращаем функцию эвристики для текущего состояния
    if (bestMove == NOT_INITIALIZED)
    {
        //std::cout << "  best move not found" << std::endl;

        int heuristic = getHeuristicEvaluation();
        prepareMap();

        return heuristic;
    }

    // Если вернулись на самый верхний уровень с просчитанными поддеревьями, то применяем лучшний ход
    if (recursiveLevel == 0 && bestMove != NOT_INITIALIZED)
    {
        //std::cout << "  best move " << bestMove << std::endl;

        // Если лучший ход - прыжок
        if (bestMove < 48)
        {
            // do jump
            QPoint diff = this->possibleMoves[4 + bestMove % 4];

            // Прыгаем
            if (draughtsman == MT_BLACK)
            {
                QPoint oldPosition = blacks[bestMove / 4];
                QPoint mid = oldPosition + diff / 2;
                int midIndex = getDraughtsmanIndexOnPosition(mid);

                this->whiteStatus[midIndex] = 0;
                this->whites[midIndex] = QPoint(-1, -1);

                this->blacks[bestMove / 4] += diff;

                std::cout << "bot jumps " << bestMove / 4 + 12
                          << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                          << " to (" << this->blacks[bestMove / 4].x() << ", "
                          << this->blacks[bestMove / 4].y() << ") and removes "
                          << midIndex << std::endl;
            }
            else
            {
                QPoint oldPosition = whites[bestMove / 4];
                QPoint mid = oldPosition + diff / 2;
                int midIndex = getDraughtsmanIndexOnPosition(mid);

                this->blackStatus[midIndex - 12] = 0;
                this->blacks[midIndex - 12] = QPoint(-1, -1);

                this->whites[bestMove / 4] += diff;

                std::cout << "bot jumps " << bestMove / 4
                          << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                          << " to (" << this->whites[bestMove / 4].x() << ", "
                          << this->whites[bestMove / 4].y() << ") and removes "
                          << midIndex << std::endl;
            }
        }
        else
        {// Если лучший ход - обычный ход
            // do move
            // Ходим
            if (draughtsman == MT_BLACK)
            {
                QPoint oldPosition = this->blacks[(bestMove - 48) / 2];

                this->blacks[(bestMove - 48) / 2] += this->possibleMoves[bestMove % 2];

                std::cout << "bot moves " << (bestMove - 48) / 2 + 12
                          << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                          << " to (" << this->blacks[(bestMove - 48) / 2].x() << ", "
                          << this->blacks[(bestMove - 48) / 2].y() << ")" << std::endl;
            }
            else
            {
                QPoint oldPosition = this->whites[(bestMove - 48) / 2];

                this->whites[(bestMove - 48) / 2] += this->possibleMoves[bestMove % 2 + 2];

                std::cout << "bot moves " << (bestMove - 48) / 2
                          << " from (" << oldPosition.x() << ", " << oldPosition.y() << ")"
                          << " to (" << this->whites[(bestMove - 48) / 2].x() << ", "
                          << this->whites[(bestMove - 48) / 2].y() << ")" << std::endl;
            }
        }
    }

    // Иначе (если в промежуточном дереве) возвращаем наверх полученную оценку для этой ветки
    return MinMax;
}

void Game::initialize()
{
    // Расставляем черные шашки на доску
    this->blacks[0] = QPoint(1, 0);
    this->blacks[1] = QPoint(3, 0);
    this->blacks[2] = QPoint(5, 0);
    this->blacks[3] = QPoint(7, 0);
    this->blacks[4] = QPoint(0, 1);
    this->blacks[5] = QPoint(2, 1);
    this->blacks[6] = QPoint(4, 1);
    this->blacks[7] = QPoint(6, 1);
    this->blacks[8] = QPoint(1, 2);
    this->blacks[9] = QPoint(3, 2);
    this->blacks[10] = QPoint(5, 2);
    this->blacks[11] = QPoint(7, 2);

    // Расставляем белые шашки на доску
    this->whites[0] = QPoint(0, 5);
    this->whites[1] = QPoint(2, 5);
    this->whites[2] = QPoint(4, 5);
    this->whites[3] = QPoint(6, 5);
    this->whites[4] = QPoint(1, 6);
    this->whites[5] = QPoint(3, 6);
    this->whites[6] = QPoint(5, 6);
    this->whites[7] = QPoint(7, 6);
    this->whites[8] = QPoint(0, 7);
    this->whites[9] = QPoint(2, 7);
    this->whites[10] = QPoint(4, 7);
    this->whites[11] = QPoint(6, 7);

    // Устанавливаем, что все шашки в игре
    for (int i = 0; i < 12; i++)
    {
        this->blackStatus[i] = 1;
        this->whiteStatus[i] = 1;
    }

    // Устанавливаем, что ни одна шашка не выбрала игроком
    selectedDraughtsman = -1;
}

void Game::debugPrint()
{
    for (int i = 0; i < 24; i++)
    {
        QPoint pos = getDraughtsmanPosition(i);

        std::cout << "  " << i << " at (" << pos.x() << ", " << pos.y() << ") " << (isInGame(i) ? "alive" : "dead") << std::endl;
    }
}
